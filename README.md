# Vue Template (non-CLI)
---
A personal boilerplate/template application I made for developing simple VueJS applications without the need for Vue-CLI (standard tooling for VueJS). 
It combines the power of useful plugins like http-vue-loader, vue-router and vuex to create applications in an organized way (still using Vue files, 
Vue router and Vuex).

## Use Cases 
---
This template is useful if you want to quickly build simple applications without worrying about how to install Node.js in your system. If you're building 
larger and serious applications, you might want to consider using Vue-CLI (the standard tooling for VueJS) as it would simplify and streamline your development 
processment from development to building to deployment. I made this template to help me integrate the power of VueJS in use-cases where a CLI might be too much
than what is necessary such as building simple application protoypes: 

## Primary Use Cases 
---
These are primary use cases I built this template for. 

1. Building on top on AdonisJS, ExpressJS, Django, ASP.NET or Laravel where integrating with the Vue-CLI might cause conflict or extra configuration. 
1. As a response to this article [https://markus.oberlehner.net/blog/goodbye-webpack-building-vue-applications-without-webpack/] by Markus Oberlehner (and as an informal update since http-vue-loader is now available which 
   allows us to directly import and use single-file Vue components without the need for loading it using webpack). 
1. Ready made template for developing applications ready with the libraries I usually use such as axios, chance, spectre (for UI), dexie, etc. 


## Primary Dependencies / Libraries Used 
---
The template application primarily uses the following VueJS libraries located in the `src/libs/vue`: 

1. **Vue Runtime (official release)** - running VueJS in the browser without CLI
1. **Vue Router (official release)** - the official VueJS SPA/MPA router for VueJS runtime.
1. **HTTP Vue Loader (official release)** - a third party plugin for allowing the use of single file components in VueJS runtime without the need for WebPack
1. **Vue Store (Vuex) (official release)**- the single-state management plugin for VueJS

**Note:** The versions of the libraries above may not be the latest version as you are reading this text. If necessary and if you prefer, you might want to replace
the library files in the folder or use a CDN provider instead for referencing the necessary scripts. Update at your own discretion, there will be a section in this 
readme that will about updating the dependencies. 

### Secondary Libraries (Picked by Repo Author)

1. **Axios** - AJAX library for JavaScript 
1. **ChanceJS** - library to generate random numbers, names, places, etc. 
1. **ChartJS** - library to create HTML5 charts with JavaScript (you might want to replace this with ApexCharts, CanvasJS, etc. (whatever suits your theme, your choice) 
1. **DexieJS** - library built on top of IndexedDB to simplify your browser-based database 
1. **jQuery** - for emergency purposes (and where necessary), **use with care**. 
1. **LessJS** - CSS simplifier/precompiler so that you don't have to code entire CSS selectors always (if you plan on migrating your workflow to Node.js if ever, you might also 
   love SASS, there's no CDN for SASS, btw, it needs a compiler). 
1. **Spectre** - UI Framework (go ahead, replace it with something that you love to use like framework7, skeleton, material, foundation, etc.)
 
 ## App Structure
 ---
 
 **General Application Structure**
 The eeneral app structure is as follows: 

1. **@root/** - Root Folder 
1. **@root/app** - This a folder for application development centered files, scripts and modules (components, views, models - if you plan on using dexie.js, etc.)   
1. **@root/res** - This is a folder where you can store application resources such as assets for images, audios, videos, fonts, etc. 
1. **@root/src** - This a reusable folder that contains library files and plugins (scripts built on top of other libraries or merging libraries). 

## Usage 
--- 
In the application's `@root/app` folder there are 6 core scripts: 

1. **app.js** - initializes and create the root Vue component (stores it in a globally accessible variable, `app.vm`)
1. **config.js** - configures the application (stores configuration object in a globally accessible variable, `app.config`) - modify and extend if necessary 
1. **database.js** - configures the application database if using dexie.js or other database provider libraries such as IndexedDB (native), WebSQL (deprecated), etc. 
1. **preload.js** - executed before app.js and after config.js, useful for loading assets such as audios, videos, images, etc. (stores loaded objects in a 
   globally accessible object, `app.preloader.objects`)
1. **router.js** - for application routing via url (can be accessed globally using `app.router`)  
1. **state.js** - creates a Vuex store for your application (can be accessed globally using `app.stores.{store_name}`)