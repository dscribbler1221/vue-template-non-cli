(function() {

    let views = {
        Splash: app.load_component("app/views/splash.vue"),
        Page1: app.load_component("app/views/page1.vue")
    };

    let components = {
        Topbar: {
            default: app.load_component("app/components/topbar/default.vue")
        }
    }

    app.router = new VueRouter({
        routes: [
            {
                path: "/",
                components: {
                    topbar: components.Topbar.default,
                    content: views.Splash
                }
            },
            {
                path: "/page1",
                components: {
                    topbar: components.Topbar.default,
                    content: views.Page1
                }
            }
        ]
    })
})();
