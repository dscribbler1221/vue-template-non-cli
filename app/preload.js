(function(app){

    /** Application Helpers **/
    app.load_component = httpVueLoader;
    app.register_component = httpVueLoader.register;

    app.preloader = {};
    app.preloader.objects = {};

    app.preloader.load_files = function() {
        return new Promise((resolve, reject)  => {

            // alternatively you can use the config object
            // to list down the files
            var files_to_load = app.config.preloader.files;

            var nfiles = files_to_load.length;
            var files_loaded = 0;

            // implement your file loader here
            function loader() {
                unit_loaded();
            }

            loader();

            // to load files properly create an unit_loaded function
            // that runs whenever a file is loaded
            function unit_loaded() {
                files_loaded++;
                if(is_finished())
                    finished();
            }

            // function to check if loading has finished
            function is_finished() {
                return (files_loaded >= nfiles)

            }

            function finished() {
                resolve();
            }
        });
    };

    app.preloader.run = function(){
        return new Promise((resolve, reject) => {
            (async function(app, resolve) {
                app.load_component = httpVueLoader;
                app.register_component = httpVueLoader.register;

                app.preloader.objects.files =
                    await app.preloader.load_files();

                resolve(app.preloader.objects);
            })(app, resolve);
        });
    }
})(app);
