(async function(window){

    let preloaded_objects = await app.preloader.run();
    
    let base_url = app.config.runtime.base_url;

    let app_data = {

    }


    app.vm = new Vue({
        el: app.config.runtime.base_context_el,
        store: app.stores.default,
        data: app_data,
        router: app.router,
        components: {
            "app-root": app.load_component("app/components/app-root.vue")
        }
    })

})(window);
