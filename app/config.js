app.config = {
    meta: {
        name: "Vue Template",
        description: "A simple template for developing Vue.js apps.",
        version: "1.0.0",
        author: "LJ Sta. Ana",
        license: "MIT License",
        website: "http://example.com/",
    },
    runtime: {
        base_url: "http://127.0.0.1:8080/",
        base_context_el: "#app"
    },
    preloader: {
        files: []
    }
}
